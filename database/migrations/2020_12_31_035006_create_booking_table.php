<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('booking_no');
            $table->date('booking_date');
            $table->decimal('balance', 8, 2);
            $table->decimal('total_price', 8, 2);
            $table->decimal('paid_amount', 8, 2);
            $table->decimal('flight_price', 8, 2);
            $table->string('flight_no');
            $table->string('departure_time');
            $table->string('arrival_time');
            $table->char('destination');
            $table->char('origin');
            $table->bigInteger('booking_city');
            $table->bigInteger('cust_id');
            $table->bigInteger('paid_by');
            $table->bigInteger('class_id');
            $table->bigInteger('status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
