<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightavailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flightavailability', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('flight_length');
            $table->string('departure_time');
            $table->string('arrival_time');
            $table->string('flight_no');
            $table->bigInteger('booked_bussiness_seats');
            $table->bigInteger('booked_economy_seats');
            $table->bigInteger('total_bussiness_seats');
            $table->bigInteger('total_economy_seats');
            $table->char('destination', 3);
            $table->char('origin', 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flightavailability');
    }
}
