<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fax', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cust_id');
            $table->char('fax_country_code', 2);
            $table->char('fax_area_code', 3);
            $table->char('fax_number', 7);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fax');
    }
}
